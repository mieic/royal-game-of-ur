module Game where

import Control.Monad (unless, when)
import Data.List (nub)
import Data.Maybe (isNothing, isJust)
import Data.Set (member, size, Set, fromList, toList)
import qualified Data.Set as S
import System.Random (Random(randomR, randoms), randomIO, StdGen, mkStdGen)
import Test.QuickCheck.Arbitrary (Arbitrary(arbitrary), Arbitrary)
import Test.QuickCheck (elements, suchThat, sample, quickCheck, verboseCheck, Property, (==>))
import Test.QuickCheck.Gen (chooseInt, elements, vectorOf)
import Text.Read (readMaybe)
import Maybes (fromJust)

-------------------------------------------------------------------------------
-- Type aliases and player datatype
-------------------------------------------------------------------------------

data Player = Player1
            | Player2
            deriving (Eq, Show)

newtype Position = Position Int
                   deriving (Eq, Ord, Show)
type Jump = Int

-------------------------------------------------------------------------------
-- State functions
-------------------------------------------------------------------------------

data SideState = SideState
  { pieces :: Set Position
  , points :: Int
  } deriving (Show)

data GameState = GameState
  { turn        :: Player
  , currentJump :: [Int]
  , sides       :: (SideState, SideState) -- (Player1, Player2)
  , gen         :: StdGen
  } deriving (Show)

maxPoints :: Int
maxPoints = 4

boardLength :: Int
boardLength = 14

opponent :: Player -> Player
opponent Player1 = Player2
opponent Player2 = Player1

side :: GameState -> Player -> SideState
side s Player1 = fst $ sides s
side s Player2 = snd $ sides s

winner :: GameState -> Maybe Player
winner s | player1Points == maxPoints = Just Player1
         | player2Points == maxPoints = Just Player2
         | otherwise                  = Nothing
  where
      player1Points = points $ fst $ sides s
      player2Points = points $ snd $ sides s

-------------------------------------------------------------------------------
-- Board functions
-------------------------------------------------------------------------------

safePosition :: Position -> Bool
safePosition = flip elem $ Position <$> [3, 7, 12]

combatPosition :: Position -> Bool
combatPosition (Position p) = 4 <= p && p <= 10

initialGame :: IO GameState
initialGame = do seed <- randomIO :: IO Int
                 pure $ rollDice GameState { turn  = Player1
                                           , currentJump = undefined
                                           , sides = (initialSide, initialSide)
                                           , gen   = mkStdGen seed
                                           }
  where
    initialSide = SideState { pieces = S.empty
                            , points = 0
                            }

-------------------------------------------------------------------------------
-- Movement functions
-------------------------------------------------------------------------------

movePositions :: Maybe Position -> Jump -> (Position, Position)
movePositions p jump = (Position initialPos, Position finalPosition)
  where
    initialPos           = case p of
      Nothing            -> -1
      Just (Position n)  -> n
    finalPosition        = initialPos + jump

possibleMove :: GameState -> Maybe Position -> Bool
possibleMove s p = validJump && validInitialPosition
                 && validFinalPosition
                 && (not checkForOponent || not invalidCapure)
                 && (isJust p || enoughPieces)
  where
    jump                        = sum $ currentJump s
    (initialPos, finalPosition) = movePositions p jump
    -- Jump is valid
    validJump            = jump > 0
    -- Enough pieces to enter, if that's the case
    remainingPieces      = maxPoints - points (side s currentPlayer)
                         - size currentPlayerPieces
    enoughPieces         = isJust p || remainingPieces > 0
    -- Initial position is valid
    currentPlayer        = turn s
    currentPlayerPieces  = pieces $ side s currentPlayer
    hasPieceInPosition   = member initialPos currentPlayerPieces
    validInitialPosition = isNothing p || hasPieceInPosition
    -- Movement is in bounds and not occupied by one of the current player's
    -- pieces
    outOfBounds          = finalPosition < Position 0
                        || finalPosition > Position boardLength
    occupiedBySelf       = member finalPosition currentPlayerPieces
    validFinalPosition   = not outOfBounds && not occupiedBySelf
    -- Valid capture, if this is the case
    opponentPieces       = pieces $ side s $ opponent currentPlayer
    checkForOponent      = combatPosition finalPosition
    occupiedByOpponent   = member finalPosition opponentPieces
    toSafePosition       = safePosition finalPosition
    invalidCapure        = occupiedByOpponent && toSafePosition

possibleMoves :: GameState -> Set Position
possibleMoves s = plays
  where
    currentPlayer        = turn s
    currentPlayerPieces  = pieces $ side s currentPlayer
    canPlayPiece piece   = possibleMove s (Just piece)
    playJust             = S.filter canPlayPiece currentPlayerPieces
    plays                = if possibleMove s Nothing
                           then S.insert (Position (-1)) playJust
                           else playJust

processAction :: GameState -> Maybe Position -> Maybe GameState
processAction s p | possibleMove s p = Just $ s' { turn = nextPlayer
                                                 , sides = sides'
                                                 }
                  | otherwise        = Nothing
  where
    nextPlayer             = if safePosition finalPos
                             then currentPlayer
                             else opponentPlayer
    s'                     = rollDice s
    jump                   = sum $ currentJump s
    (initialPos, finalPos) = movePositions p jump
    currentPlayer          = turn s
    opponentPlayer         = opponent currentPlayer
    currentPlayerSide      = side s currentPlayer
    opponentPlayerSide     = side s opponentPlayer
    currentPlayerPieces    = pieces currentPlayerSide
    opponentsPlayerPieces  = pieces opponentPlayerSide
    -- Insert the piece if it doesn't go out of the game
    currentPlayerPieces'   =
      let removedPiece = S.delete initialPos currentPlayerPieces
      in doIfPoint removedPiece (S.insert finalPos) id
    -- Capture the opponent's piece, if that's the case
    opponentsPlayerPieces' = if combatPosition finalPos
                             then S.delete finalPos opponentsPlayerPieces
                             else opponentsPlayerPieces
    -- Check if there's a new point
    currentPlayerPoints    = points currentPlayerSide
    currentPlayerPoints'   = doIfPoint currentPlayerPoints id (+1)
    doIfPoint x f g = if finalPos < Position 14
                      then f x
                      else g x
    -- Create sides
    currentPlayerSide'     = SideState
      { pieces = currentPlayerPieces'
      , points = currentPlayerPoints'
      }
    opponentPlayerSide'    = opponentPlayerSide
      { pieces = opponentsPlayerPieces'
      }
    createSides Player1 x y = (x, y)
    createSides Player2 x y = (y, x)
    sides' = createSides currentPlayer currentPlayerSide' opponentPlayerSide'

skipPlayer :: GameState -> IO GameState
skipPlayer s = pure . rollDice $ s { turn = opponent $ turn s }

-------------------------------------------------------------------------------
-- Play functions
-------------------------------------------------------------------------------

rollDice :: GameState -> GameState
rollDice s = s { currentJump = [d1, d2, d3, d4]
               , gen         = g4
               }
  where
    g0 = gen s
    (d1, g1) = randomR (0, 1) g0
    (d2, g2) = randomR (0, 1) g1
    (d3, g3) = randomR (0, 1) g2
    (d4, g4) = randomR (0, 1) g3

play :: GameState -> IO ()
play s | isJust (winner s) = putStrLn "Game over!"
       | otherwise         = do
  let s' = rollDice s
  let jump = sum $ currentJump s'
  print jump
  print (possibleMoves s')
  when (null $ possibleMoves s') (do putStrLn "No valid moves!"
                                     s' <- skipPlayer s'
                                     play s')
  l <- getLine
  let parsedPos = readMaybe l :: Maybe (Maybe Int)
  when (isNothing parsedPos) (do putStrLn "Invalid input!"; play s)
  let pos = fromJust parsedPos
  let maybeS'' = processAction s' (Position <$> pos)
  when (isNothing maybeS'') (do putStrLn "Invalid move!"; play s)
  let s'' = fromJust maybeS''
  print s''
  play s''

-------------------------------------------------------------------------------
-- QuickCheck interfaces
-------------------------------------------------------------------------------

instance Arbitrary Position where
  arbitrary = Position <$> chooseInt (0, 13)

instance Arbitrary Player where
  arbitrary = elements [Player1, Player2]

instance Arbitrary GameState where
  arbitrary = do
    p  <- arbitrary
    j  <- vectorOf 4 (chooseInt (0, 1))
    s1 <- arbitrarySide maxPoints
    s2 <- arbitrarySide $ maxPoints
                        - (if points s1 == maxPoints then 1 else 0)
    i  <- arbitrary
    return $ GameState { turn = p
                       , currentJump = j
                       , sides = (s1, s2)
                       , gen = mkStdGen i
                       }
    where
      arbitrarySide max = do
        points <- elements [0..max]
        list   <- vectorOf (maxPoints - points)
                           (Position <$> chooseInt (0, 13))
        return $ SideState { pieces = fromList list
                           , points = points
                           }

-------------------------------------------------------------------------------
-- QuickCheck tests
-------------------------------------------------------------------------------

-- It is impossible for two players to have the necessary score to win the
-- game at the same time
prop_noTwoWinners :: GameState -> Maybe Position -> Property
prop_noTwoWinners s p = isJust s' ==> winners < 2
  where score s = points . side s <$> [Player1, Player2]
        winners = length (filter (>=maxPoints) $ score (fromJust s'))
        s'      = processAction s p

-- The sum of the values from the rolled dice is a number between 0 and 4
prop_maxJump :: GameState -> Bool
prop_maxJump s = value >= 0 && value <= 4
  where value = sum $ currentJump $ rollDice s

-- Trying to play an invalid move should not result in a new board
prop_invalidMove :: GameState -> Maybe Position -> Property
prop_invalidMove s m = not (possibleMove s m) ==>
                       isNothing $ processAction s m

--
prop_possibleMoves :: GameState -> Bool
prop_possibleMoves s = validPositions && notTooMany && noDuplicates
                    && validPieces && noCollissions
  where posMoves         = possibleMoves s
  -- The possible-to-move pieces are on valid positions
        validPositions   = all (\(Position x) -> x > -2 && x < 14)
                               (toList posMoves)
  -- Not able to move more pieces than actually exist
        notTooMany       = maxPoints >= length  (toList posMoves)
  -- A piece should not be counted multiple times in possibleMoves
        noDuplicates     = toList posMoves == nub (toList posMoves)
  -- The possible-to-move pieces are actually pieces that player has
        validPieces      = all (`elem` currentPieces) posMovesOnBoard
  -- possibleMoves does not cause a collision that is not a capture
        noCollissions    = (currJump <= 0 || null (toList posMoves))
                        || not (all (`elem` piecesOnBoard)
                                    (Position <$> finPositions))
        currJump         = sum $ currentJump s
        finPositionsOnBoard = [n | n <- finPositions, n < 14]
        finPositions     = [n + currJump | Position n <- toList posMoves]
        posMovesOnBoard  = toList $ S.filter (> Position (-1)) posMoves
        piecesOnBoard    = currentPieces
                        ++ [ n | n <- opponentsPiecesNotInCombat
                               , n > Position 3 ]
        currentPieces    = toList $ S.filter (< Position 14)
                                  $ pieces $ side s (turn s)
        opponentsPiecesNotInCombat = toList $ S.filter
                                     (`elem` [Position 3, Position 7])
                                     (pieces $ side s (opponent $ turn s))

-- processAction does not cause two winners at the same time,
-- nor cause points which are negative or above maxPoints
prop_processAction :: GameState -> Maybe Position -> Bool
prop_processAction s p | isJust newState = notTwoWinners && validPoints
                       | otherwise = True
  where newState       = processAction s p
        ns             = fromJust newState
        validPoints    = playerPoints > (-1) && opponentPoints > (-1)
                      && playerPoints < (maxPoints + 1)
                      && opponentPoints < (maxPoints + 1)
        notTwoWinners  = playerPoints < maxPoints || opponentPoints < maxPoints
        currPlayer     = turn ns
        playerPoints   = points $ side ns currPlayer
        currOpponent   = opponent currPlayer
        opponentPoints = points $ side ns currOpponent
