module Main where

import           Control.Monad
import           Data.IORef
import           Data.Maybe (isJust, fromJust, isNothing)
import           Data.Set (member)
import qualified Graphics.UI.Threepenny      as UI
import           Graphics.UI.Threepenny.Core

import           Game

updateRef :: Maybe Position -> IORef GameState -> IO ()
updateRef pos s = do
  state <- liftIO $ readIORef s
  let mstate' = processAction state pos
  when (isJust mstate') (liftIO $ writeIORef s (fromJust mstate'))

-------------------------------------------------------------------------------
-- Cell creation
-------------------------------------------------------------------------------

type UpdateFunction a = IORef GameState -> IO a

getCell :: IORef GameState -> Maybe (UpdateFunction a) -> String
        -> [(String, String)] -> UI Element
getCell s mf text style = do
  elem <- UI.td # set UI.text text # set UI.style realStyle
  when (isJust mf) (on UI.click elem $ \_ -> liftIO $ fromJust mf s)
  pure elem
  where
    realStyle = [ ("border"         , "1px solid black")
                , ("height"         , "2em"            )
                , ("width"          , "2em"            )
                , ("padding"        , "0"              )
                , ("text-align"     , "center"         )
                , ("vertical-align" , "middle"         )
                , ("cursor"         , "default"        )
                , ("user-select"    , "none"           )
                ] ++ style

getEmptyCell :: IORef GameState -> Maybe (UpdateFunction a) -> UI Element
getEmptyCell s f | isNothing f = getCell s Nothing " " style
                 | otherwise   = do
                    state <- liftIO $ readIORef s
                    getBoxCell s f Nothing (Just $ turn state) style
  where
    style = [("border", "0")]

getBoxCell :: IORef GameState -> Maybe (UpdateFunction a) -> Maybe Position
           -> Maybe Player -> [(String, String)] -> UI Element
getBoxCell s f _   Nothing  style = getCell s Nothing " " style
getBoxCell s f pos (Just p) style = do
  state <- liftIO $ readIORef s
  getCell s f "\9865" (options state ++ style
                    ++ [("text-shadow", "0 0 2px " ++ shadowColour)])
  where
    baseColour   = if p == Player1 then "#000000" else "#FFFFFF"
    shadeColour  = if p == Player1 then "#222222" else "#DDDDDD"
    shadowColour = if p == Player1 then "#FFFFFF" else "#000000"
    options   s  = if possibleMove s pos && p == turn s
                   then [("color", baseColour), ("cursor", "pointer")]
                   else [("color", shadeColour)]

getSafeBoxCell :: IORef GameState -> Player -> Position -> UI Element
getSafeBoxCell s player pos = do
  state <- liftIO $ readIORef s
  let f = if isPossibleMove state
          then Just $ updateRef (Just pos)
          else Nothing
  getBoxCell s f (Just pos) (occupiedBy state) [("background-color", style)]
  where
    isPossibleMove s = player == turn s && member pos (possibleMoves s)
    playerPieces   s = pieces $ side s player
    occupiedBy     s = if member pos $ playerPieces s
                       then Just player else Nothing
    style            = if safePosition pos then "#b58863" else "#f0d9b5"

getCombatBoxCell :: IORef GameState -> Position -> UI Element
getCombatBoxCell s pos = do
  state <- liftIO $ readIORef s
  let f = if isPossibleMove state
          then Just $ updateRef (Just pos)
          else Nothing
  getBoxCell s f (Just pos) (occupiedBy state) [("background-color", style)]
  where
    occupiedByPlayer1 s = member pos $ pieces $ side s Player1
    occupiedByPlayer2 s = member pos $ pieces $ side s Player2
    occupiedByTurn    s = (occupiedByPlayer1 s && turn s == Player1)
                       || (occupiedByPlayer2 s && turn s == Player2)
    isPossibleMove    s = occupiedByTurn s && member pos (possibleMoves s)
    occupiedBy        s = case (occupiedByPlayer1 s, occupiedByPlayer2 s) of
                               (True, _   ) -> Just Player1
                               (_   , True) -> Just Player2
                               _            -> Nothing
    style            = if safePosition pos then "#b58863" else "#f0d9b5"

-------------------------------------------------------------------------------
-- Table and row creation
-------------------------------------------------------------------------------

getRow :: [UI Element] -> UI Element
getRow cells = do
  cells <- sequenceA cells
  UI.tr # set UI.children cells # set UI.style style
  where
    style = []

getExtremeRow :: IORef GameState -> Player -> UI Element
getExtremeRow s player = do
  s <- liftIO $ readIORef s
  getRow (leftCells ++ middleCells s ++ rightCells)
  where
    emptyCell          = getEmptyCell s
    getPositionCells   = (getSafeBoxCell s player <$>)
    leftCells          = getPositionCells $ Position <$> [3, 2, 1, 0]
    canBringNewPiece s = possibleMove s Nothing
    f                s = if player == turn s && canBringNewPiece s
                         then Just $ updateRef Nothing
                         else Nothing
    middleCells      s = [emptyCell $ f s, emptyCell Nothing]
    rightCells         = getPositionCells $ Position <$> [13, 12]

getMiddleRow :: IORef GameState -> UI Element
getMiddleRow s = getRow cells
  where
    getPositionCells = (getCombatBoxCell s <$>)
    cells            = getPositionCells $ Position <$> [4..11]

getBoardTable :: IORef GameState ->  UI Element
getBoardTable s = do
  rs <- sequenceA rows
  UI.table # set UI.children rs # set UI.style style
  where
    rows  = [getExtremeRow s Player1, getMiddleRow s, getExtremeRow s Player2]
    style = [("border-collapse", "collapse")]

-------------------------------------------------------------------------------
-- Scoreboard and dice
-------------------------------------------------------------------------------

getScoreBoard :: GameState -> UI Element
getScoreBoard s = do
  player1 <- player1
  x <- score " x "
  player2 <- player2
  UI.span # set UI.children [player1, x, player2]
          # set UI.style [ ("display"        , "flex"  )
                         , ("flex-direction" , "column")
                         , ("justify-content", "center")
                         , ("margin-left"    , "1em"   )
                         , ("flex-basis"     , "10%"   )
                         ]
  where
    player1 = (score . show . points . side s) Player1
    player2 = (score . show . points . side s) Player2
    score n = UI.span # set UI.text n
                      # set UI.style [ ("display"        , "flex"  )
                                     , ("justify-content", "center")
                                     ]

getDice :: GameState -> UI Element
getDice s = do
  dice <- sequenceA $ makeDice <$> dice
  UI.span # set UI.children dice
          # set UI.style [ ("display"        , "flex"  )
                         , ("flex-direction" , "column")
                         , ("justify-content", "center")
                         , ("margin-left"    , "1em"   )
                         , ("flex-basis"     , "10%"   )
                         ]
  where
    showDie 0 = "△"
    showDie 1 = "◬"
    makeDice n = UI.span # set UI.text (showDie n)
                         # set UI.style [ ("display"        , "flex"  )
                                        , ("justify-content", "center")
                                        ]
    dice       = currentJump s

-------------------------------------------------------------------------------
-- Main function
-------------------------------------------------------------------------------

main :: IO ()
main = do
  startGUI defaultConfig
    { jsPort       = Just 8080
    , jsStatic     = Just "../wwwroot"
    } setup

setTable :: Window -> IORef GameState -> UI ()
setTable window ref = do
  game       <- liftIO $ readIORef ref
  liftIO $ print game
  scoreBoard <- getScoreBoard game
  dice       <- getDice game
  elem       <- getBoardTable ref
  div <- UI.div # set UI.children [elem, dice, scoreBoard]
                # set UI.style [ ("display"       , "flex")
                               , ("flex-direction", "row" )
                               , ("font-size"     , "4em" )
                               ]
  getBody window # set UI.children [div]
                 # set UI.style [ ("display"       , "flex"      )
                                , ("flex-direction", "column"    )
                                , ("font-family"   , "sans-serif")
                                ]
  let gameOver = winner game
  if isNothing gameOver
  then do
    when (null $ possibleMoves game) (do
      let note = UI.span # set UI.text ("There are no valid movements, "
                                     ++ "click anywhere to continue!")
                         # set UI.style [ ("text-align", "center")
                                        , ("width"     , "100%"  )
                                        , ("font-size" , "2em"   )
                                        , ("margin-top", "1em"   )
                                        ]
      getBody window #+ [note]
      on UI.click elem $ \_ -> do
        game' <- liftIO $ skipPlayer game
        liftIO $ writeIORef ref game')
    on UI.click elem $ \_ -> do
      setTable window ref
      delete elem
      delete dice
      delete scoreBoard
  else do
    let note = UI.span # set UI.text (showPlayer (fromJust gameOver)
                                   ++ " is the winner!")
                       # set UI.style [ ("text-align", "center")
                                      , ("width"     , "100%"  )
                                      , ("font-size" , "2em"   )
                                      , ("margin-top", "1em"   )
                                      ]
    getBody window #+ [note]
    pure ()
  where
    showPlayer Player1 = "Player 1"
    showPlayer Player2 = "Player 2"

setup :: Window -> UI ()
setup window = do pure window # set UI.title "Royal Game of Ur!"
                  gameState    <- liftIO initialGame
                  gameStateRef <- liftIO $ newIORef gameState
                  setTable window gameStateRef